import pydis as pydis
import pydis2 as pydis2

def disort(TAU_GAS=0.1,TAU_CLD=0,g=0.8,ssa=1.0,albedo=0,MU0=1.0,NSTR=4,MU=1, PHI=0, ILAY=1):    
    foo=pydis.dis_rad(TAU_GAS,TAU_CLD,g,ssa,albedo,MU0,NSTR,MU,PHI,ILAY+1);
    if 'numpy' in str(type(foo)):
        if foo.shape[0]<=1.5:
            foo=foo[0,:];
        elif foo.shape[1]<=1.5:
            foo=foo[:,0];
    return foo


def disort2(TAU_GAS=0.1,TAU_CLD=0,g=0.8,ssa=1.0,albedo=0,MU0=1.0,NSTR=4,MU=1,PHI=0, ILAY=1):    
#    nlyr=len(TAU_GAS);    
#    if len(TAU_CLD)<1.5: TAU_CLD=[TAU_CLD]*nlyr;
#    if len(g)<1.5: g=[g]*nlyr;
#    if len(ssa)<1.5: ssa=[ssa]*nlyr;  
    foo=pydis2.dis_rad(TAU_GAS,TAU_CLD,g,ssa,albedo,MU0,NSTR,MU,PHI,ILAY+1)
    if 'numpy' in str(type(foo)):
        foo=foo[:,0]; 
    return foo