import numpy as np
import warnings

def rayleigh_tau(w):
    tau=0.002152*(1.0456-341.291*w**(-2)-0.9023*w**2)/(1+0.002706*w**(-2)-85.9686*w**2);
    return tau;
    
def rayleigh_t(w):
    t=0.001448*(1.0456-341.291*w**(-2)-0.9023*w**2)/(1+0.002706*w**(-2)-85.9686*w**2);
    return t;

  