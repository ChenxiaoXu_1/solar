import h5py as h5

def print_name(name):    
    print(name)
    
def print_h5_var(fn):
    hdf = h5.File(fn, 'r')
    hdf.visit(print_name)
    hdf.close()    

def print_h5_attrs(hdf):
    for item in hdf.attrs.keys():
        print(item + ":", hdf.attrs[item])