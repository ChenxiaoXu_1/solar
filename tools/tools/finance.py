from datetime import datetime
import pandas as pd
import requests
import numpy as np
import json
import scipy.stats as ss


def tail(f, lines_2find=20, str_mode=False):  
    f.seek(0, 2)                         #go to end of file
    bytes_in_file = f.tell()             
    lines_found, total_bytes_scanned = 0, 0
    while lines_2find+1 > lines_found and bytes_in_file > total_bytes_scanned: 
        byte_block = min(1024, bytes_in_file-total_bytes_scanned)
        f.seek(bytes_in_file-(byte_block+total_bytes_scanned), 0)
        total_bytes_scanned += byte_block
        lines_found += f.read(1024).count('\n')
    f.seek(bytes_in_file-total_bytes_scanned, 0);
    if str_mode:
        f.readline();
        line_list=f.readlines();
    else:
        f.readline();
        line_list=np.loadtxt(f,delimiter=',');
    return line_list[-lines_2find:];

def get_intraday_data(symbol, interval_seconds=301, num_days=10):
    # Specify URL string based on function inputs.
    url_string = 'http://www.google.com/finance/getprices?q={0}'.format(symbol.upper())
    url_string += "&i={0}&p={1}d&f=d,o,h,l,c,v".format(interval_seconds,num_days)

    # Request the text, and split by each line
    r = requests.get(url_string).text.split()

    # Split each line by a comma, starting at the 8th line
    r = [line.split(',') for line in r[7:]]
    
###output as an numpy array
#    df=r;
#    for i,line in enumerate(df):
#        df[i][0]=line[0][1:];
#    df=np.array(r).astype('float32');    

    # Save data in Pandas DataFrame
#    tt=[t.pop(0) for t in r];
    tt=[];
    for t in r:
        # Convert UNIX to Datetime format
        foo=datetime.fromtimestamp(int(t.pop(0)[1:]));
        tt.append(foo);
    df = pd.DataFrame(r, columns=['Close','High','Low','Open','Volume'])
    df.index=tt;

    return df
    
def get_intraday_array(symbol, interval_seconds=301, num_days=10):
    # Specify URL string based on function inputs.
    url_string = 'http://www.google.com/finance/getprices?q={0}'.format(symbol.upper())
    url_string += "&i={0}&p={1}d&f=d,o,h,l,c,v".format(interval_seconds,num_days)

    # Request the text, and split by each line
    r = requests.get(url_string).text.split()

    # Split each line by a comma, starting at the 8th line
    r = [line.split(',') for line in r[7:]]
    
###output as an numpy array
    for i,line in enumerate(r):
        r[i][0]=line[0][1:];
    for i,line in enumerate(r):        
        if len(r[i])<5:
            del r[i];
    df=np.array(r).astype('float32');
    return df    
        
from googlefinance import getQuotes
def quote_realtime(symbols):
    nsym=len(symbols); isym=0; 
    price=[]; symbs=[]; 
    while isym<nsym:
        aa=getQuotes(symbols[isym:isym+99]);
        price.extend([float(a['LastTradePrice'].replace(',','')) for a in aa]);
        symbs.extend([a['StockSymbol'] for a in aa]);
        isym+=99;
    return price,symbs    

def premarket(symbol, exchange):
    link    = "http://finance.google.com/finance/info?client=ig&q="
    url     = link + "%s:%s" % (exchange, symbol)
    content = requests.get(url).text
    info    = json.loads(content[3:])[0]
#    ts      = str(info["elt"])    # time stamp
#    last    = float(info["l"])    # close price (previous trading day)
    pre     = float(info["el"])   # stock price in pre-market (after-hours)
#    return (ts, last, pre)
    return pre


def bull_bear(C,H,L,PC=5,PH=34,PL=13):
    import talib
    
    emaC            = talib.MA(C, timeperiod=PC, matype=1);
    emaH            = talib.MA(H, timeperiod=PH, matype=1);
    emaL            = talib.MA(L, timeperiod=PL, matype=1); 
    bull            = emaC - emaH
    bear            = emaL - emaC   
    
    return bull,bear 


#Black and Scholes
def d1(S0, K, r, sigma, T):
    return (np.log(S0/K) + (r + sigma**2 / 2) * T)/(sigma * np.sqrt(T))
 
def d2(S0, K, r, sigma, T):
    return (np.log(S0 / K) + (r - sigma**2 / 2) * T) / (sigma * np.sqrt(T))
 
def BlackScholes(type,S0, K, r, sigma, T):
    if type=="C":
        return S0 * ss.norm.cdf(d1(S0, K, r, sigma, T)) - K * np.exp(-r * T) * ss.norm.cdf(d2(S0, K, r, sigma, T))
    else:
       return K * np.exp(-r * T) * ss.norm.cdf(-d2(S0, K, r, sigma, T)) - S0 * ss.norm.cdf(-d1(S0, K, r, sigma, T))