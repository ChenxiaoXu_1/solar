import numpy as np
# cimport numpy as np

cdef extern from "multi_thresholding.cpp":
    void thresholding2(float src[],int pixelsCount, float minv, float maxv, float thresh[2])
    void thresholding3(float src[],int pixelsCount, float minv, float maxv, float thresh[3])

# def threshold2(float[:] src, float[:] thresh, minv=None, maxv=None):
#     n=len(src)
#     if minv is None:
#         minv=np.nanmin(src);
#     if maxv is None:
#         maxv=np.nanmax(src);
#     thresholding2(&src[0],n,minv,maxv,&thresh[0])
# 
# def threshold3(float[:] src, float[:] thresh, minv=None, maxv=None):
#     n=len(src)
#     if minv is None:
#         minv=np.nanmin(src);
#     if maxv is None:
#         maxv=np.nanmax(src);
#     thresholding3(&src[0],n,minv,maxv,&thresh[0])

def threshold2(float[:] src, minv=None, maxv=None):
    n=len(src)
#     cdef np.ndarray[np.float32_t, ndim=1, mode='c'] thresh = np.empty((2,),dtype=np.float32)
    cdef float[2] thresh;
    if minv is None:
        minv=np.nanmin(src);
    if maxv is None:
        maxv=np.nanmax(src);
    thresholding2(&src[0],n,minv,maxv,thresh)
    return thresh

def threshold3(float[:] src, minv=None, maxv=None):
    n=len(src)
    cdef float[3] thresh;
    if minv is None:
        minv=np.nanmin(src);
    if maxv is None:
        maxv=np.nanmax(src);
    thresholding3(&src[0],n,minv,maxv,thresh)
    return thresh
