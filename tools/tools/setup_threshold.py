from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
# import numpy

# sourcefiles = ['example_cy.pyx']
# extensions = [Extension("example_cy", sourcefiles)]
# setup(ext_modules = cythonize(extensions))

sourcefiles = ['threshold.pyx']
extensions = [Extension("threshold", sourcefiles)]
setup(ext_modules = cythonize(extensions, language = "c++"))
# setup(ext_modules = cythonize(extensions, language = "c++"), include_dirs=[numpy.get_include()])
