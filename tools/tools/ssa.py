import numpy as np


def ssa_reconstruct(dd,nlag=10,nreg=1):
    nt=dd.size-nlag+1;  ###K
    df=np.zeros((nt,nlag));
    for i in range(nlag):
        df[:,i]=dd[i:(i+nt)]; 
    u,w,v=np.linalg.svd(df);  
    
    rc_d=np.zeros(nt+nlag-1);

    rc=np.zeros_like(df[:,:]);
    for ip in range(nreg):
        rc+=w[ip]*np.outer(u[:,ip],v[ip,:])[:,:];        
    i = 0;
    for id in range(-(nt-1),nlag):
        rc_d[i] = np.mean(np.diag(np.flipud(rc),id));
        i += 1;
        
    return rc_d;

def ssa_forecast(dd,L=10,horizon=1,reg=1,reconstruct=False):
    K=dd.size-L+1;
    df=np.zeros((L,K));
    for i in range(K):
        df[:,i]=dd[i:(i+L)]; 
    u,w,v=np.linalg.svd(df);  
    
    if isinstance(reg, int):
        reg=np.arange(reg);
    rc_d=np.zeros(K+L-1+horizon);
    if reconstruct:
        rc=np.zeros_like(df);
        for ip in reg:
            rc+=w[ip]*np.outer(u[:,ip],v[ip,:]);        
        i = 0;
        for id in range(-(L-1),K):
            rc_d[i] = np.mean(np.diag(np.flipud(rc),id));
#             rc_d[i] = np.mean(np.diag((rc),id));
            i += 1;
    else:   
        rc_d[:K+L-1]=dd[:K+L-1];

    P=u[:,reg].T;
    A=np.sum(P[:,-1]*P[:,:-1].T,axis=1);
    nu=np.sum(P[:,-1]**2);     
    A/=(1-nu);

    for i in range(horizon):
        rc_d[i+K+L-1]=np.sum(A*rc_d[(i+K):(i+K+L-1)]);
        
    return rc_d,w;   

# def ssa_forecast(dd,nlag=10,len=1,nreg=1,recon=False):
#     nt=dd.size-nlag;
#     df=np.zeros((nt,nlag));
#     for i in range(nlag):
#         df[:,i]=dd[i:(i+nt)]; 
#     u,w,v=np.linalg.svd(df);  
#     
#     rc_d=np.zeros(nt+nlag+len-1);
#     if recon:
#         rc=np.zeros_like(df[:,:]);
#         for ip in range(nreg):
#             rc+=w[ip]*np.outer(u[:,ip],v[ip,:])[:,:];        
#         i = 0;
#         for id in range(-(nt-1),nlag):
#             rc_d[i] = np.mean(np.diag(np.flipud(rc),id));
#             i += 1;
#     else:   
#         rc_d[:nt+nlag-1]=dd[:nt+nlag-1];
# 
#     P=v[:nreg,:nlag];
#     A=np.sum(P[:,-1]*P[:,:-1].T,axis=1);
#     nu=np.sum(P[:,-1]**2);     
#     A/=(1-nu);
# 
#     for i in range(len):
#         rc_d[i+nt+nlag-1]=np.sum(A*rc_d[(i+nt):(i+nt+nlag-1)]);
#         
#     return rc_d;    