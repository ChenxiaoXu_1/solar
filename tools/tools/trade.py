import numpy as np
from scipy.stats import linregress
import talib

class Map(dict):
    """
    Example:
    m = Map({'first_name': 'Eduardo'}, last_name='Pool', age=24, sports=['Soccer'])
    """
    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.iteritems():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.iteritems():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]

def position_value(context,data,day):
    positions=context.positions; day_map=data.day_map;
    value=0;
    for ipos,pos in enumerate(positions):
        isym=pos[0];   
        if day_map[isym][day]<-0.5:
            for itmp in range(day-1,0,-1):
                if day_map[isym][itmp]>-0.5:   
                    C=data.d[isym][day_map[isym][itmp],3];
                    break;
        else:
            C=data.d[isym][day_map[isym][day],3];
        value+=C*pos[1];  
    return value;              

def open_position(context,data,day):
    symbols=context.symbols; positions=context.positions;
    for isym, symbol in enumerate(symbols):
        if context.day_map[isym][day]<-0.5: continue;
        iday=context.day_map[isym][day];
        symbol=symbols[isym];
        if symbol in context.blacklist: continue;
        istart=max(0,iday-100);
        dd=context.d[isym][istart:iday+1,:];    
        if len(dd)<=5: continue;
        O=dd[:,0]; H=dd[:,1]; L=dd[:,2]; C=dd[:,3]; V=dd[:,4];        
        if min_prev[isym]<0 or min_prev[isym] not in C:
            min_prev[isym]=np.min(C);
        elif C[-1]<=min_prev[isym]:
            min_prev[isym]=C[-1];
            continue;
        fmin=min_prev[isym];      
       
        if max_prev[isym]<0:       
            if C[-2]>=C[-1] and C[-2]>=C[-3] and C[-2]>=inc*fmin:
                max_prev[isym]=C[-2];   
            continue;
#        if max_prev[isym]<inc*fmin or max_prev[isym] not in C[30:]:       
#            if C[-2]>=C[-1] and C[-2]>=C[-3] and C[-2]>=inc*fmin:
#                max_prev[isym]=C[-2];   
#            else:
#                max_prev[isym]=-1; 
#            continue; 
        if cash<1: continue;    
        flag=True;    
        for pos in positions:
            if pos[0]==isym and pos[2]==max_prev[isym]:
               flag=False;
               break;   
        high=max_prev[isym];
#        if flag and np.random.randint(0,20)>18:
#            buy=O[-1]; 
        if flag and O[-1]>=high:                
###            buy=max(O[-1],high);
            buy=C[-1];            
            vol=min(cash,max(cash/nbin,3000))/buy;
            cash-=vol*buy;
            positions.append([isym,vol,buy,day]); 
#            print('Buy:',vol,symbols[isym],dates[day],buy);
            ntrans+=1;
            imax=np.argmax(C>=max_prev[isym]);
            min_prev[isym]=min(C[imax:]);
            max_prev[isym]=-1;

def close_position(context,data,day):
    positions=context.positions;
    if len(positions)<0.5: return;
    for pos in positions[:]:
        if day<=pos[3]: continue;
        isym=pos[0];   
        if context.day_map[isym][day]<-0.5: continue;
        iday=context.day_map[isym][day];
        dd=context.d[isym][iday,:];    
        O=dd[0]; H=dd[1]; L=dd[2]; C=dd[3]; V=dd[4];
        high=pos[2];
        if day-pos[3]>=max_hold:
            cash+=C*pos[1];
#            print('Sell:',pos[1],symbols[isym],dates[pos[3]],pos[2],dates[day],C);
            positions.remove(pos);
        elif C<stop_loss*high:
#            sell=min(stop_loss*high,O);
#            sell=stop_loss*high;
            sell=C;
            cash+=sell*pos[1];
            loss+=1;
#            print('Sell:',pos[1],symbols[isym],dates[pos[3]],pos[2],dates[day],sell);
            positions.remove(pos); 
        elif C>profit_point*high:
            cash+=C*pos[1];
            gain+=1;
#            print('Sell:',pos[1],symbols[isym],dates[pos[3]],pos[2],dates[day],C);
            positions.remove(pos);    
    
def open_position_gap(context,data,day):
    positions=context.positions; symbols=data.symbols;
    d=data.d; day_map=data.day_map;
    for isym, symbol in enumerate(symbols):
        if context.cash<100: break;
        if day_map[isym][day]<-0.5: continue;
        iday=day_map[isym][day];
        symbol=symbols[isym];
        if symbol in context.blacklist: continue;
        istart=max(0,iday-3);
        dd=d[isym][istart:iday+1,:];    
        if len(dd)<=3: continue;
        O=dd[:,0]; H=dd[:,1]; L=dd[:,2]; C=dd[:,3]; V=dd[:,4];        
            
        if isym in [row[0] for row in positions]:
             continue;  
        if L[-2]>1.03*O[-1]:                
            buy=O[-1];            
            vol=min(context.cash,max(context.cash/context.nbin,3000))/buy;
            context.cash-=vol*buy;
            context.cash-=5;
            positions.append([isym,vol,buy,day]); 
            
#            fig,ax = plt.subplots(1);
#            tmp=d[isym][:,:4];
#            tmp=np.column_stack((np.arange(len(tmp)),tmp));
#            candlestick_ohlc(ax,tmp,width=0.6);
#            ax.scatter(iday,buy,marker='o',color='m',s=28,zorder=2);                   
#            fig.savefig(outpath+symbol+str(dates[day])+'.jpg');
#            plt.close(fig);    
            
#            print('Buy:',vol,symbols[isym],data.dates[day],buy,C[-1],L[-2]);
            record='Buy: '+str(vol)+' '+symbol+' @ '+str(buy)+' on '+str(data.dates[day]);
            context.history.append(record);
            context.ntrans+=1;

def open_position_gap2(context,data,day):
    
    positions=context.positions; symbols=data.symbols;
    d=data.d; day_map=data.day_map;
    buy_list=[];
    for isym, symbol in enumerate(symbols):        
        if day_map[isym][day]<-0.5: continue;
        iday=day_map[isym][day];
        symbol=symbols[isym];
        if symbol in context.blacklist: continue;
        istart=max(0,iday-5);
        dd=d[isym][istart:iday+1,:];    
        if len(dd)<=5: continue;
        O=dd[:,0]; H=dd[:,1]; L=dd[:,2]; C=dd[:,3]; V=dd[:,4];                  
        if isym in [row[0] for row in positions]:
             continue;               
        if L[-2]>1.03*O[-1] and L[-2]<10.8*O[-1]:
#            y=C[:-1]/C[-2];     
#            reg=linregress(np.arange(len(y)),y);
#            if reg.slope<-0.005: continue;
            buy_list.append(isym);
    nbuy=len(buy_list);        
    if nbuy<0.5: return; 
    nbuy=min(nbuy,3);   buy_list=buy_list[:nbuy];     
    quota=(context.cash-nbuy*5)/nbuy;        
    for isym in buy_list: 
        symbol=symbols[isym];
        iday=day_map[isym][day];
        symbol=symbols[isym];
        istart=max(0,iday-3);
        dd=d[isym][istart:iday+1,:];    
        O=dd[:,0];         
        buy=O[-1];            
        vol=quota/buy;
        context.cash-=vol*buy;
        context.cash-=5;
        positions.append([isym,vol,buy,day]); 
        record='Buy: '+str(vol)+' '+symbol+' @ '+str(buy)+' on '+str(data.dates[day]);
        context.history.append(record);
        context.ntrans+=1;

def close_position_gap(context,data,day):
    positions=context.positions; stop_loss=context.stop_loss;
    d=data.d; day_map=data.day_map;
    if len(positions)<0.5: return;
    for pos in positions[:]:
            isym=pos[0];   
            if day_map[isym][day]<-0.5: continue;
            iday=day_map[isym][day];
            dd=d[isym][iday,:];    
            O=dd[0]; L=dd[2]; C=dd[3];
            if day<=pos[3]: 
                if L<stop_loss*O:
                    sell=stop_loss*O;  
                else:
                    sell=C;                       
            elif day>pos[3]+30:
                sell=C;
            else:
                continue;
            context.cash+=sell*pos[1];
            context.cash-=5;
            if sell>pos[2]:
                context.positive+=1;
            else:
                context.negative+=1;
#            print('Sell:',pos[1],symbols[isym],dates[pos[3]],pos[2],dates[day],C);
            record='Sell: '+str(pos[1])+' '+data.symbols[isym]+' @ '+str(sell)+' on '+str(data.dates[day]);
            context.history.append(record);
            positions.remove(pos);  
                
def open_position_engulf(context,data,day):
    positions=context.positions; symbols=data.symbols;
    d=data.d; day_map=data.day_map;
    for isym, symbol in enumerate(symbols):
        if context.cash<100: break;
        if day_map[isym][day]<-0.5: continue;
        iday=day_map[isym][day];
        symbol=symbols[isym];
        if symbol in context.blacklist: continue;
        istart=max(0,iday-4);
        dd=d[isym][istart:iday+1,:];    
        if len(dd)<4.5: continue;
        O=dd[:,0]; H=dd[:,1]; L=dd[:,2]; C=dd[:,3]; V=dd[:,4];        
            
        if isym in [row[0] for row in positions] or C[-1]/O[-1]>0.95:
             continue;  
        val=talib.CDLENGULFING(O,H,L,C);  
        if abs(val[-1])<1: continue;
        y=C/C[-1];
        reg=linregress(np.arange(len(y)),y);
        if reg.slope<-0.01:             
            buy=C[-1];            
            vol=min(context.cash,max(context.cash/context.nbin,3000))/buy;
            context.cash-=vol*buy;
            context.cash-=5;
            positions.append([isym,vol,buy,day]); 
            
            record='Buy: '+str(vol)+' '+symbol+' @ '+str(buy)+' on '+str(data.dates[day]);
            context.history.append(record);
            context.ntrans+=1;    

def open_position_engulf2(context,data,day):
    positions=context.positions; symbols=data.symbols;
    d=data.d; day_map=data.day_map;
    buy_list=[];
    for isym, symbol in enumerate(symbols):
        if day_map[isym][day]<-0.5: continue;
        iday=day_map[isym][day];
        symbol=symbols[isym];
        if symbol in context.blacklist: continue;
        istart=max(0,iday-4);
        dd=d[isym][istart:iday+1,:];    
        if len(dd)<4.5: continue;
        O=dd[:,0]; H=dd[:,1]; L=dd[:,2]; C=dd[:,3]; V=dd[:,4];        
            
        if isym in [row[0] for row in positions] or C[-1]/O[-1]>0.95:
             continue;  
        val=talib.CDLENGULFING(O,H,L,C);  
        if abs(val[-1])<1: continue;
        y=C/C[-1];
        reg=linregress(np.arange(len(y)),y);
        if reg.slope<-0.01:             
            buy_list.append(isym);
    nbuy=len(buy_list);        
    if nbuy<0.5: return; 
    nbuy=min(nbuy,3); buy_list=buy_list[:nbuy];    
    quota=(context.cash-nbuy*5)/nbuy;       
    for isym in buy_list: 
        symbol=symbols[isym];
        iday=day_map[isym][day];
        buy=d[isym][iday,3];    
        vol=quota/buy;
        context.cash-=vol*buy;
        context.cash-=5;
        positions.append([isym,vol,buy,day]); 
        
        record='Buy: '+str(vol)+' '+symbol+' @ '+str(buy)+' on '+str(data.dates[day]);
        context.history.append(record);
        context.ntrans+=1; 
            
def close_position_engulf(context,data,day):
    positions=context.positions;
    d=data.d; day_map=data.day_map;
    if len(positions)<0.5: return;
    for pos in positions[:]:
            isym=pos[0];   
            if day_map[isym][day]<-0.5: continue;
            iday_buy=day_map[isym][pos[3]];            
            iday=day_map[isym][day];
            if iday<iday_buy+2: continue;
            dd=d[isym][iday,:];    
            O=dd[0]; L=dd[2]; C=dd[3];
            if any(d[isym][iday_buy+1:iday+1,3]<context.stop_loss*pos[2]):
                sell=context.stop_loss*pos[2];
            else:
                sell=C;
            context.cash+=sell*pos[1];
            context.cash-=5;
            if sell>pos[2]:
                context.positive+=1;
            else:
                context.negative+=1;
#            print('Sell:',pos[1],symbols[isym],dates[pos[3]],pos[2],dates[day],C);
            record='Sell: '+str(pos[1])+' '+data.symbols[isym]+' @ '+str(sell)+' on '+str(data.dates[day]);
            context.history.append(record);                
            positions.remove(pos);              