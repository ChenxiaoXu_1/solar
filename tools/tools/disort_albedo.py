import pydis_albedo

def disort(TAU_GAS, TAU_CLOUD, MU0=1.0, NSTR=4, gg=0.8):    
    return pydis_albedo.dis_rad(TAU_GAS, TAU_CLOUD, MU0, NSTR, gg, 1, 1.0)
