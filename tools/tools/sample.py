## sample.py
#import ctypes
#import os
#
## Try to locate the .so file in the same directory as this file
#_file = 'sample.dll'
#_path = os.path.join(*(os.path.split(__file__)[:-1] + (_file,)))
#_mod = ctypes.cdll.LoadLibrary(_path)
#
## int gcd(int, int)
#gcd = _mod.gcd
#gcd.argtypes = (ctypes.c_int, ctypes.c_int)
#gcd.restype = ctypes.c_int
#
## int in_mandel(double, double, int)
#in_mandel = _mod.in_mandel
#in_mandel.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_int)
#in_mandel.restype = ctypes.c_int
#
## int divide(int, int, int *)
#_divide = _mod.divide
#_divide.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_int))
#_divide.restype = ctypes.c_int
#
#def divide(x, y):
#    rem = ctypes.c_int()
#    quot = _divide(x, y, rem)
#    return quot,rem.value
#
## void avg(double *, int n)
## Define a special type for the 'double *' argument
#class DoubleArrayType:
#    def from_param(self, param):
#        typename = type(param).__name__
#        if hasattr(self, 'from_' + typename):
#            return getattr(self, 'from_' + typename)(param)
#        elif isinstance(param, ctypes.Array):
#            return param
#        else:
#            raise TypeError("Can't convert %s" % typename)
#
#    # Cast from lists/tuples
#    def from_list(self, param):
#        val = ((ctypes.c_double)*len(param))(*param)
#        return val
#
#    from_tuple = from_list
#
#    # Cast from a numpy array
#    def from_ndarray(self, param):
#        return param.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
#
##DoubleArray = DoubleArrayType()
#_avg = _mod.avg
#_avg.argtypes = (DoubleArrayType(), ctypes.c_int)
#_avg.restype = ctypes.c_double
#
#def avg(values):
#    return _avg(values, len(values))
#
## struct Point { }
#class Point(ctypes.Structure):
#    _fields_ = [('x', ctypes.c_double),
#                ('y', ctypes.c_double)]
#
## double distance(Point *, Point *)
#distance = _mod.distance
#distance.argtypes = (ctypes.POINTER(Point), ctypes.POINTER(Point))
#distance.restype = ctypes.c_double


# sample.py
from cffi import FFI
import os


ffi = FFI()

# Try to locate the .so file in the same directory as this file
_file = 'sample.dll'
_path = os.path.join(*(os.path.split(__file__)[:-1] + (_file,)))
_mod = ffi.dlopen(_path)

# Describe the data type and function prototype to cffi.
ffi.cdef('''
double avg(double *a, int n);
double avg2d(double **a, int n, int m);
''')

def avg(values):
    pa = ffi.cast("double *", values.ctypes.data)
    return _mod.avg(pa, len(values))

def avg2d(values):
    pa = ffi.new("double* [%d]" % values.shape[0]) 
    for i in range(len(values)):
        pa[i] = ffi.cast("double *", values[i,:].ctypes.data)
    return _mod.avg2d(pa, values.shape[0],values.shape[1])
