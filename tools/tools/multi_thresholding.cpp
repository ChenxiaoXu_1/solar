// cv::Mat&amp; src - source image's matrix
void thresholding2(float src[],int pixelsCount, float minv, float maxv, float thresh[2])
{
    int histogram[256] = { 0 };
    float coef = 255.0/(maxv-minv);
    int idx;
    for (int i = 0; i< pixelsCount; i++)
    {   
        idx = (src[i]-minv)*coef+0.5;
        if (idx<0) idx=0;
        else if (idx>255) idx=255;
        histogram[idx]++;
    }

    double c = 0;
    double Mt = 0;

    double p[256] = { 0 };
    for (int i = 0; i < 256; i++)
    {
        p[i] = (double) histogram[i] / (double) pixelsCount;
        Mt += i * p[i];
    }

    double maxBetweenVar = 0;

    double w0 = 0;
    double m0 = 0;
    double c0 = 0;
    double p0 = 0;

    double w1 = 0;
    double m1 = 0;
    double c1 = 0;
    double p1 = 0;

    for (int tr1 = 0; tr1 < 256; tr1++)
    {
        p0 += p[tr1];
        w0 += (tr1 * p[tr1]);
        if (p0 != 0)
        {
            m0 = w0 / p0;
        }

        c0 = p0 * (m0 - Mt) * (m0 - Mt);

        c1 = 0;
        w1 = 0;
        m1 = 0;
        p1 = 0;
        for (int tr2 = tr1 + 1; tr2 < 256; tr2++)
        {

            p1 += p[tr2];
            w1 += (tr2 * p[tr2]);
            if (p1 != 0)
            {
                m1 = w1 / p1;
            }

            c1 = p1 * (m1 - Mt) * (m1 - Mt);


            double p2 = 1 - (p0 + p1);
            double w2 = Mt - (w0 + w1);
            double m2 = w2 / p2;
            double c2 = p2 * (m2 - Mt) * (m2 - Mt);

            double c = c0 + c1 + c2;

            if (maxBetweenVar < c)
            {
                maxBetweenVar = c;
                thresh[0] = tr1;
                thresh[1] = tr2;
            }
        }
    }
    for (int i=0; i<2; i++) 
        thresh[i] = thresh[i]/coef + minv;
}

void thresholding3(float  src[],int pixelsCount, float minv, float maxv, float thresh[3])
{
    int histogram[256] = { 0 };
    float coef = 255.0/(maxv-minv);
    int idx;
    for (int i = 0; i< pixelsCount; i++)
    {   
        idx = (src[i]-minv)*coef+0.5;
        if (idx<0) idx=0;
        else if (idx>255) idx=255;
        histogram[idx]++;
    }
    
    double c = 0;
    double Mt = 0;

    double p[256] = { 0 };
    for (int i = 0; i < 256; i++)
    {
        p[i] = (double) histogram[i] / (double) pixelsCount;
        Mt += i * p[i];
    }


    double maxBetweenVar = 0;

    double w0 = 0;
    double m0 = 0;
    double c0 = 0;
    double p0 = 0;

    double w1 = 0;
    double m1 = 0;
    double c1 = 0;
    double p1 = 0;

    double w2 = 0;
    double m2 = 0;
    double c2 = 0;
    double p2 = 0;
    for (int tr1 = 0; tr1 < 256; tr1++)
    {
        p0 += p[tr1];
        w0 += (tr1 * p[tr1]);
        if (p0 != 0)
        {
            m0 = w0 / p0;
        }

        c0 = p0 * (m0 - Mt) * (m0 - Mt);

        c1 = 0;
        w1 = 0;
        m1 = 0;
        p1 = 0;
        for (int tr2 = tr1 + 1; tr2 < 256; tr2++)
        {

            p1 += p[tr2];
            w1 += (tr2 * p[tr2]);
            if (p1 != 0)
            {
                m1 = w1 / p1;
            }

            c1 = p1 * (m1 - Mt) * (m1 - Mt);


            c2 = 0;
            w2 = 0;
            m2 = 0;
            p2 = 0;
            for (int tr3 = tr2 + 1; tr3 < 256; tr3++)
            {

                p2 += p[tr3];
                w2 += (tr3 * p[tr3]);
                if (p2 != 0)
                {
                    m2 = w2 / p2;
                }

                c2 = p2 * (m2 - Mt) * (m2 - Mt);

                double p3 = 1 - (p0 + p1 + p2);
                double w3 = Mt - (w0 + w1 + w2);
                double m3 = w3 / p3;
                double c3 = p3 * (m3 - Mt) * (m3 - Mt);

                double c = c0 + c1 + c2 + c3;

                if (maxBetweenVar < c)
                {
                    maxBetweenVar = c;
                    thresh[0] = tr1;
                    thresh[1] = tr2;
                    thresh[2] = tr3;
                }
            }
        }
    }
    for (int i=0; i<3; i++) 
        thresh[i] = thresh[i]/coef + minv;
}
