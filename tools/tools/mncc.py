import numpy as np
# from matplotlib import pyplot as plt
import pyfftw

class FFTW:
    def __init__(self, shape, dtype, threads=1, inverse=False):
        self.threads = threads

        if inverse:
            self.arr_in = pyfftw.n_byte_align_empty(shape[0], pyfftw.simd_alignment, dtype=dtype)
            self.fftw = pyfftw.builders.irfft2(self.arr_in, shape[1],
                                               threads=threads, avoid_copy=True)
        else:
            self.arr_in = pyfftw.n_byte_align_empty(shape, pyfftw.simd_alignment, dtype=dtype)
            self.fftw = pyfftw.builders.rfft2(self.arr_in, overwrite_input=True,
                                              threads=threads, avoid_copy=True)

    def get_inverse(self):
        arr_out = self.fftw.get_output_array()
        return FFTW(shape=(arr_out.shape, self.arr_in.shape), dtype=arr_out.dtype, threads=self.threads, inverse=True)

    def __call__(self, arr, reverse=False):
        step = -1 if reverse else 1
        self.arr_in[arr.shape[0]:, :] = 0
        self.arr_in[:arr.shape[0], arr.shape[1]:] = 0
        self.arr_in[:arr.shape[0], :arr.shape[1]] = arr[::step, ::step]
        ret = self.fftw()
        return ret

def _next_regular(target):
    """
    Copied from scipy.

    Find the next regular number greater than or equal to target.
    Regular numbers are composites of the prime factors 2, 3, and 5.
    Also known as 5-smooth numbers or Hamming numbers, these are the optimal
    size for inputs to FFTPACK.

    Target must be a positive integer.
    """
    if target <= 6:
        return target

    # Quickly check if it's already a power of 2
    if not (target & ( target -1)):
        return target

    match = float('inf')  # Anything found will be smaller
    p5 = 1
    while p5 < target:
        p35 = p5
        while p35 < target:
            # Ceiling integer division, avoiding conversion to float
            # (quotient = ceil(target / p35))
            quotient = -(-target // p35)

            # Quickly find next power of 2 >= quotient
            p2 = 2** ((quotient - 1).bit_length())

            N = p2 * p35
            if N == target:
                return N
            elif N < match:
                match = N
            p35 *= 3
            if p35 == target:
                return p35
        if p35 < match:
            match = p35
        p5 *= 5
        if p5 == target:
            return p5
    if p5 < match:
        match = p5
    return match


class Convolver:
    def __init__(self, shape_first, shape_second, dtype, threads=1):
        self.shape_first = shape_first
        self.shape_second = shape_second

        self.dtype = np.dtype(dtype)

        self.shape_result = np.array(self.shape_first) + self.shape_second - 1
        self.slice_result = tuple([slice(0, int(d)) for d in self.shape_result])
        self.shape_fft = [_next_regular(int(d)) for d in self.shape_result]

        self.fftw = FFTW(shape=self.shape_fft, dtype=self.dtype, threads=threads)
        self.ifftw = self.fftw.get_inverse()

        self.arrays = {}
        self.ffts = {}
        self.convolutions = {}

    def add_array(self, name, arr):
        if name in self.arrays:
            for k in self.arrays[name][1][0]:
                del self.ffts[k]
            for k in self.arrays[name][1][1]:
                del self.convolutions[k]
        assert arr.dtype == self.dtype
        assert arr.shape in (self.shape_first, self.shape_second)
        self.arrays[name] = (arr, ([], []))

    def _fft(self, name, reverse):
        key = (name, reverse)
        if key not in self.ffts:
            self.ffts[key] = self.fftw(self.arrays[name][0], reverse).copy()
            self.arrays[name][1][0].append(key)
        return self.ffts[key]

    def _convolve(self, name_first, name_second, correlate):
        key = (name_first, name_second, correlate)
        if key not in self.convolutions:
            fft = self._fft(name_first, reverse=False) * self._fft(name_second, reverse=correlate)
            ret = self.ifftw(fft)
            self.convolutions[key] = ret[self.slice_result].copy()

            self.arrays[name_first][1][1].append(key)
            self.arrays[name_second][1][1].append(key)
        return self.convolutions[key]

    def convolve(self, name_first, name_second):
        return self._convolve(name_first, name_second, False)

    def correlate(self, name_first, name_second):
        return self._convolve(name_first, name_second, True)


def _init_mask(template, mask):
    if mask is None:
        mask = np.ones_like(template)
    return mask.astype(template.dtype)

def mncc(image, tmpl, mask1=None, mask2=None, threads=1, ratio=0.25):
    """
    Computes the Weighted Normalized Cross Correlation.

    :param image: First array - image.
    :param template: Second array - template.
    :param mask: Mask containing weights for template pixels. Same size as template.
    :param threads: Number of threads (passed to FFTW).
    :return: The resulting cross correlation.

    """
    mask1 = _init_mask(image, mask1)
    mask2 = _init_mask(tmpl, mask2)
    img1 =  (image * mask1)
    img2 = (tmpl * mask2)
    
    convolver = Convolver(image.shape, tmpl.shape, threads=threads, dtype=image.dtype)
    convolver.add_array('image', img1)
    convolver.add_array('tmpl', img2)
    convolver.add_array('mask1', mask1)
    convolver.add_array('mask2', mask2)
    convolver.add_array('image_sq', img1 ** 2)
    convolver.add_array('tmpl_sq', img2 ** 2)
    
    b=convolver.correlate('image', 'mask2')
    c=convolver.correlate('mask1', 'tmpl')
    d = convolver.correlate('mask1', 'mask2'); d[d<1]=1
    nom=convolver.correlate('image', 'tmpl')-b*c/d;
    denom1=convolver.correlate('image_sq', 'mask2')-b**2/d;
    denom2=convolver.correlate('mask1','tmpl_sq')-c**2/d;

    result = nom / np.sqrt(denom1 * denom2)
#     plt.figure(); plt.imshow(nom,vmin=-1e7,vmax=3e7); plt.colorbar();
#     plt.figure(); plt.imshow(np.sqrt(denom1 * denom2)); plt.colorbar();

    result[d<ratio*np.sum(mask2)] = float('nan')

    return result
