import numpy as np
import os, glob
from matplotlib import pyplot as plt
import stat_tools as st
from PIL import Image
import geopy
from geopy.distance import VincentyDistance
from scipy.interpolate import RegularGridInterpolator

def IlluAdjst(Him, Rim):
    
    Hadj = np.zeros(Him.shape, dtype = np.uint8)

    for i in range(0, 3):
        HimMask = Him[:,:,i] > 0
        HimMask = 1 * HimMask
        HimVec = np.reshape(Him[:,:,i], (Him.shape[0] * Him.shape[1], -1))
        RimVec = np.reshape(Rim[:,:,i], (Rim.shape[0] * Rim.shape[1], -1))

        R = np.histogram(RimVec, bins = np.arange(0,257))[0]
        H = np.histogram(HimVec, bins = np.arange(0,257))[0]
   
        Rc = np.cumsum(R)
        Hc = np.cumsum(H)    
        
        Hmap = [np.where(Rc >= val)[0][0] for val in Hc]
    
        for j in range(0, Him.shape[1]):
            Hadj[:,j,i] = [Hmap[val] for val in Him[:,j,i]]
        
        Hadj[:,:,i] = Hadj[:,:,i]*HimMask
            
    return Hadj

###################################################
#Values of latitude and longitude pairs form a tuple,
#where the index 0 corresponds to latitude and index 1
#corresponds to longitude
###################################################
Cam_Coor_all = {'HD815_1': [40.87203321,  -72.87348295],
               'HD815_2':  [40.87189059,  -72.873687],
               'HD490'  :  [40.865968816, -72.884647222], 
               'HD17'   :  [40.8575056,   -72.8547344], 
               'HD19'   :  [40.8580088,   -72.8575717], 
               'HD20'   :  [40.85785,     -72.8597] }




GHI_Coor = {'GHI1':   [40.868972, -72.852225],
            'GHI2':   [40.868116, -72.851999],
            'GHI3':   [40.867261, -72.851758],
            'GHI4':   [40.866331, -72.851655],
            'GHI5':   [40.865166, -72.851309],
            'GHI6':   [40.863690, -72.851217],
            'GHI7':   [40.867781, -72.849226],
            'GHI8':   [40.866068, -72.849014],
            'GHI9':   [40.864829, -72.849031],
            'GHI10':   [40.862745, -72.850047],
            'GHI11':   [40.858548, -72.846888],
            'GHI12':   [40.857791, -72.848877],
            'GHI13':   [40.857964, -72.850339],
            'GHI14':   [40.859147, -72.852050],
            'GHI15':   [40.857746, -72.851804],
            'GHI16':   [40.858624, -72.854309],
            'GHI17':   [40.857478, -72.854772],
            'GHI18':   [40.857970, -72.856379],
            'GHI19':   [40.857982, -72.857620],
            'GHI20':   [40.857826, -72.859741],
            'GHI21':   [40.858323, -72.863776],
            'GHI22':   [40.859174, -72.864268],
            'GHI23':   [40.859951, -72.864739],
            'GHI24':   [40.860966, -72.865434],
            'GHI25':   [40.862072, -72.865909]}

#Cam_Coor_all = [(40.865968816,-72.884647222)] 
CamID_all = ['HD815_1', 'HD490', 'HD17']


inpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180604/data/undistort_output/'
outpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180604/data/image_stitching_adjust_GHI_output/'


if not os.path.isdir(outpath):
    os.makedirs(outpath)
    os.chmod(outpath,0o755)  


#cloud base height all
CBH_all = 1500
day = '20180310'

deg2rad = np.pi/180
max_theta = 70 * deg2rad     
max_tan = np.tan(max_theta)


####set up paths, constantsand initial parameters
#inpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180502/data/undistorted/'     
#outpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180502/data/image_stitched/'     


#Set up DestImage
#Destination Image Shape
DestImg_shp = (9000, 9000, 3)
#Dest Physical Coverage
DestPhysCov = (18, 18)
#Destination Image
DestImg = np.zeros(DestImg_shp, dtype = np.uint8)

#Destination Center Coordinates
DestCen_Coor = (40.88, -72.87)
#Destination Converage Per Pixel in km
DestCovPPix_km = (DestPhysCov[0] / DestImg_shp[0], DestPhysCov[1] / DestImg_shp[1])#DestCenter
DestCen = geopy.Point(DestCen_Coor[0], DestCen_Coor[1])
#DestCoveragePerPixel_Coords
DestCovPPix_Coor = (VincentyDistance(kilometers = DestCovPPix_km[0]).destination(DestCen, 0).latitude - DestCen_Coor[0],
                VincentyDistance(kilometers = DestCovPPix_km[1]).destination(DestCen, 90).longitude - DestCen_Coor[1])
#Destnation Start
DestStt = (DestCen_Coor[0] - DestImg_shp[0]/2 * DestCovPPix_Coor[0],
          DestCen_Coor[1] - DestImg_shp[1]/2 * DestCovPPix_Coor[1])
#Destnation End
DestEnd = (DestCen_Coor[0] + DestImg_shp[0]/2 * DestCovPPix_Coor[0],
          DestCen_Coor[1] + DestImg_shp[1]/2 * DestCovPPix_Coor[1])

#DestGrid = (np.arange(DestStt[0], DestEnd[0], DestCovPPix_Coor[0]), np.arange(DestStt[1], DestEnd[1], DestCovPPix_Coor[1]))  

DestGrid = (np.linspace(DestStt[0], DestEnd[0], DestImg_shp[0], endpoint = True),
        np.linspace(DestStt[1], DestEnd[1], DestImg_shp[1], endpoint = True))

Mu = (np.mean(DestGrid[0]), np.mean(DestGrid[1]))
Sigma = (np.std(DestGrid[0]), np.std(DestGrid[1]))

DestGrid = ((DestGrid[0] - Mu[0]) / Sigma[0], (DestGrid[1] - Mu[1]) / Sigma[1])
DestStt = (DestGrid[0][0], DestGrid[1][0])
DestEnd = (DestGrid[0][-1], DestGrid[1][-1])
#Coordinates vs km ratio of \Destination Image Coverage Per Pixel
DestCovPPix_Cood_km_ratio = (DestCovPPix_Coor[0]/ DestCovPPix_km[0], DestCovPPix_Coor[1]/ DestCovPPix_km[1])


RefImg_name = inpath + 'HD815_1' + '/' + day + '/' + 'HD815_1_20180310180014.jpg'
RefImg = plt.imread(RefImg_name)
RefImg = np.flip(RefImg, 0)
RefImg = np.flip(RefImg, 1)

filepath = inpath + CamID_all[0] + '/' + day + '/' '*jpg'
flist = sorted(glob.glob(filepath))

counter = 0


for f in flist:    
    #Destination Image
    if (counter == 1):
        break
    counter = counter + 1 
    DestImg = np.zeros(DestImg_shp, dtype = np.uint8)        
    for i in range(0, len(CamID_all)):
        OrigFileName = os.path.basename(f)
        CamID = CamID_all[i]
        CamCoor = (Cam_Coor_all[CamID][0], Cam_Coor_all[CamID][1])
        Img_name = inpath  + CamID_all[i] + '/' + day + '/' + CamID_all[i] + '_' + OrigFileName[-18:]

        CamImg = plt.imread(Img_name)
        CamImg = np.flip(CamImg, 0)
        CamImg = np.flip(CamImg, 1)
    
        if (Img_name != RefImg_name):
            CamImg = IlluAdjst(CamImg, RefImg)    
      
        print("working on the time stamp:")
        print(OrigFileName[-18:-4])
        print('for camera')
        print(CamID)
    
        #plt.imshow(CamImg, origin = 'lowest')
        a = CamImg[0:1000, 0:2000]
        #plt.imshow(a, origin = 'lowest')
        CamImg_shp = CamImg.shape
        CBH = CBH_all

        #Camera Coverage in km
        CamCov_km = 2 *0.001 * CBH * max_tan
        #Camera Coverage per pixel in km        
        CamCovPPix_km = (CamCov_km / CamImg_shp[0], CamCov_km / CamImg_shp[1])
        #Camera Coverage per pixel in coordinates
        CamCovPPix_Coor = (CamCovPPix_km[0] * DestCovPPix_Cood_km_ratio[0], CamCovPPix_km[1] * DestCovPPix_Cood_km_ratio[1])
        #Cam Image Start
        CamStt = (CamCoor[0] - (CamImg_shp[0] -1)/2 * CamCovPPix_Coor[0], CamCoor[1] - (CamImg_shp[1] -1)/2 * CamCovPPix_Coor[1])
        #Cam Image End
        CamEnd = (CamCoor[0] + (CamImg_shp[0] -1)/2 * CamCovPPix_Coor[0], CamCoor[1] + (CamImg_shp[1] -1)/2 * CamCovPPix_Coor[1])

        #CamGrid = (np.arange(CamStt[0], CamEnd[0], CamCovPPix_Coor[0]), np.arange(CamStt[1], CamEnd[1], CamCovPPix_Coor[1]))  

        CamGrid = (np.linspace(CamStt[0], CamEnd[0], CamImg_shp[0], endpoint = True),
                   np.linspace(CamStt[1], CamEnd[1], CamImg_shp[1], endpoint = True))


        CamGrid = ((CamGrid[0] - Mu[0]) / Sigma[0], (CamGrid[1] - Mu[1])/Sigma[1])  

        CamStt= (CamGrid[0][0], CamGrid[1][0])
        CamEnd = (CamGrid[0][-1], CamGrid[1][-1])
        #Destination Camera Patch Index
        DestIndLat = np.where((DestGrid[0] > CamStt[0]) & (DestGrid[0] < CamEnd[0]))[0]
        DestIndLon = np.where((DestGrid[1] > CamStt[1]) & (DestGrid[1] < CamEnd[1]))[0]
    
        if (len(DestIndLat) != len(DestIndLon)):
            if (len(DestIndLat) > len(DestIndLon)):
                DestIndLat = DestIndLat[0: len(DestIndLon)]   
            else:
                DestIndLon = DestIndLon[0: len(DestIndLat)]
            
        DestCamPatInd = (DestIndLat, DestIndLon)
            
#    DestCamPatInd= (np.where((DestGrid[0] > CamStt[0]) & (DestGrid[0] < CamEnd[0]))[0],
#                np.where((DestGrid[1] > CamStt[1]) & (DestGrid[1] < CamEnd[1]))[0])
    
        DestCamPatInd_stt = (DestCamPatInd[0][0], DestCamPatInd[1][0])
        DestCamPatInd_end = (DestCamPatInd[0][-1]+1, DestCamPatInd[1][-1]+1)    

        DestCamPat = (DestGrid[0][DestCamPatInd_stt[0] : DestCamPatInd_end[0]],
                      DestGrid[1][DestCamPatInd_stt[1] : DestCamPatInd_end[1]])
    
        assert (DestCamPat[0][0] > CamGrid[0][0])
        assert (DestCamPat[0][-1] < CamGrid[0][-1])
        assert (DestCamPat[1][0] > CamGrid[1][0])
        assert (DestCamPat[1][-1] < CamGrid[1][-1])

        DestCamPat_Grid_Long, DestCamPat_Grid_Lati = np.meshgrid(DestCamPat[1], DestCamPat[0])
        DestCamPat_Grid_Long = np.reshape(DestCamPat_Grid_Long, -1)
        DestCamPat_Grid_Lati = np.reshape(DestCamPat_Grid_Lati, -1)

        DestCamPat_Grid_Coor = np.stack((DestCamPat_Grid_Lati, DestCamPat_Grid_Long)).transpose()
    
    
        a = np.reshape(DestCamPat_Grid_Lati, (DestCamPat[1].shape[0], DestCamPat[0].shape[0]))

    
        for j in range(0, 3):

            Interpolation = RegularGridInterpolator(CamGrid, CamImg[:,:,j])
            DestImgVal = Interpolation(DestCamPat_Grid_Coor)
            DestImgVal = DestImgVal.astype(np.uint8)
 
            DestImg_Ori = DestImg[DestCamPatInd_stt[0]: DestCamPatInd_end[0], DestCamPatInd_stt[1]: DestCamPatInd_end[1],j]
            DestImg_Ori = DestImg_Ori.reshape(-1)
            Ori_Nonzero_Index = np.nonzero(DestImg_Ori)
    
            if (len(Ori_Nonzero_Index[0]) > 0):
                DestImgVal[Ori_Nonzero_Index] = DestImg_Ori[Ori_Nonzero_Index]
        
#           DestImgVal = np.reshape(DestImgVal, (DestCamPat[0].shape[0], DestCamPat[1].shape[0]))
            DestImgVal = np.reshape(DestImgVal, (DestCamPat[1].shape[0], DestCamPat[0].shape[0]))
        
        
#            plt.imshow(DestImgVal, origin = 'lowest')
#            DestImgVal = np.flip(DestImgVal, 0)
#            DestImgVal = np.flip(DestImgVal, 1)        
            DestImg[DestCamPatInd_stt[0]: DestCamPatInd_end[0], DestCamPatInd_stt[1]: DestCamPatInd_end[1],j] = DestImgVal
    
        
#    DestImg = np.flip(DestImg, 0)
#    FinalImage = Image.fromarray(DestImg, origin = 'lowest')
    plt.imshow(DestImg, origin = 'lowest')
    plt.axis('off')
    
    for k in range(1, 2):
        sensor = 'GHI' + str(k)
        Sen_Coor = (GHI_Coor[sensor][0], GHI_Coor[sensor][1])
        Sen_Coor = ((Sen_Coor[0] - Mu[0])/Sigma[0], (Sen_Coor[1] - Mu[1])/Sigma[1])
        Mark_Lati = np.where(DestGrid[0] >= Sen_Coor[0])[0][0]
        Mark_Long = np.where(DestGrid[1] >= Sen_Coor[1])[0][0]   
        plt.scatter(Mark_Long, Mark_Lati, s=1, c='yellow', marker='o')
    
    savename = outpath + day + '/' + OrigFileName[-18:] + '.jpg'
    plt.imsave(savename)
#    FinalImage.save(savename)






