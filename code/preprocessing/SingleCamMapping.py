import numpy as np
import os, glob
from matplotlib import pyplot as plt
import stat_tools as st
from PIL import Image
import geopy
from geopy.distance import VincentyDistance
from scipy.interpolate import RegularGridInterpolator

deg2rad = np.pi/180

def SingleCamMapping(CamCoor,
                     DestCen_Coor,
                     theta = 70,
                     CBH,
                     DestCovPPix_Coor,
                     DestImg_shp,
                     DestStt,
                     DestEnd,
                     CamImg_shp,
                     DestCovPPix_Coor_km_ratio
                    )

	#maximum zenith angle used for processing
	max_theta = 70 * deg2rad     
	max_tan = np.tan(max_theta)
    #Camera Coverage in km
	CamCov_km = 2 * CBH * max_tan
    #Camera Coverage per pixel in km        
	CamCovPPix_km = (0.001*CamCov / CamImg_shp[0], 0.001*CamCov / CamImg_shp[1])
    #Camera Coverage per pixel in coordinates
	CamCovPPix_Coor = (CamCovPPix_km[0] * DestCovPPix_Coor_km_ratio[0], CamCovPPix_km[1] * DestCovPPix_Coor_km_ratio[1])
    #Cam Image Start
	CamStt = (CamCoor[0] - (CamImg_shp[0] -1)/2 * CamCovPPix_Coor[0], CamCoor[1] - (CamImg_shp[1] -1)/2 * CamCovPPix_Coor[1])
    #Cam Image End
	CamEnd = (CamCoor[0] + (CamImg_shp[0] -1)/2 * CamCovPPix_Coor[0], CamCoor[1] + (CamImg_shp[1] -1)/2 * CamCovPPix_Coor[1])

	CamGrid = (np.arange(CamStt[0], CamEnd[0], CamCovPPix_Coor[0])  
			np.arange(CamStt[1], CamEnd[1], CamCovPPix_Coor[1])  

	DestGrid = (np.arange(DestStt[0], DestEnd[0], DestCovPPix_Coor[0]),
			 np.arange(DestStt[1], DestEnd[1], DestCovPPix_Coor[1])[0:-1])  


	Mu = (np.mean(DestGrid[0]), np.mean(DestGrid[1])
	Simga = (np.std(DestGrid[0]), np.std(DestGrid[1])

	DestGrid = ((DestGrid[0] - Mu[0]) / Sigma[0], (DestGrid[1] - Mu[1])/Sigma[1])
	CamGrid = ((CamGrid[0] - Mu[0]) / Sigma[0], (CamGrid[1] - Mu[1])/Sigma[1])	

	CamStt= (CamGrid[0][0], CamGrid[1][0])
	CamEnd = (CamGrid[0][-1], CamGrid[1][-1])
    #Destination Camera Patch Index 
	DestCamPatIndex= (np.where((DestGrid[0] > CamStart[0]) & (DestGrid[0] < CamEnd[0]))[0],
						 np.where((DestGrid[1] > CamStart[1]) & (DestGrid[1] < CamEnd[1]))[0])

	DestCamPatchIndex_start = (DestCamPatchIndex[0][0], DestCamPatchIndex[1][0])
	DestCamPatchIndex_end = (DestCamPatchIndex[0][-1]+1, DestCamPatchIndex[1][-1]+1)

	return DestCamPatchIndex_start, DestCamPatchIndex_end, CamGrid






