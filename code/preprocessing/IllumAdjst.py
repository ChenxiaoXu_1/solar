#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 14:20:58 2018

@author: chenxiaoxu
"""
import numpy as np

def IlluAdjst(Him, Rim):
    
    Hadj = np.zeros(Him.shape, dtype = np.uint8)

    for i in range(0, 3):
        HimMask = Him[:,:,i] > 0
        HimMask = 1 * HimMask
        HimVec = np.reshape(Him[:,:,i], (Him.shape[0] * Him.shape[1], -1))
        RimVec = np.reshape(Rim[:,:,i], (Rim.shape[0] * Rim.shape[1], -1))

        R = np.histogram(RimVec, bins = np.arange(0,257))[0]
        H = np.histogram(HimVec, bins = np.arange(0,257))[0]
   
        Rc = np.cumsum(R)
        Hc = np.cumsum(H)    
        
        Hmap = [np.where(Rc >= val)[0][0] for val in Hc]
    
        for j in range(0, Him.shape[1]):
            Hadj[:,j,i] = [Hmap[val] for val in Him[:,j,i]]
        
        Hadj[:,:,i] = Hadj[:,:,i]*HimMask
            
    return Hadj



    