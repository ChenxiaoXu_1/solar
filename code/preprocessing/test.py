#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  7 16:29:19 2018

@author: chenxiaoxu
"""

#import numpy as np
#import matplotlib.pyplot as plt
#from scipy.interpolate import griddata
#from scipy.interpolate import interp1d
#def func(x, y):
#    return x*(1-x)*np.cos(4*np.pi*x) * np.sin(4*np.pi*y**2)**2
#
#grid_x, grid_y = np.mgrid[0:1:100j, 0:1:200j]
#points = np.random.rand(1000,2)
#values = func(points[:,0], points[:,1])
#
#grid_z0 = griddata(points, values, (grid_x, grid_y), method='nearest')
#grid_z1 = griddata(points, values, (grid_x, grid_y), method='linear')
#grid_z2 = griddata(points, values, (grid_x, grid_y), method='cubic')
#
#plt.subplot(221)
#plt.plot(points[:,0], points[:,1], 'k.', ms = 1)
#plt.subplot(222)
#plt.imshow(grid_z0.T, extent=(0,1,0,1), origin='lower')
#plt.subplot(223)
#plt.imshow(grid_z1.T, extent=(0,1,0,1), origin='lower')
#plt.subplot(224)
#plt.imshow(grid_z2.T, extent=(0,1,0,1), origin='lower')

#x = np.array([3,4,1,34,676,21])
#a =np.where( (x > 3) & (x < 30))[0]

#x = np.linspace(0, 10, num = 11, endpoint = True)
#y = np.cos(-x**2 / 9.0)
#f = interp1d(x, y)
#f2 = interp1d(x, y, kind = 'cubic')
#
#xnew = np.linspace(0, 10, num = 41, endpoint = True)
#plt.plot(x, y, 'o', xnew, f(xnew), '-', xnew, f2(xnew), '--')


#from scipy.interpolate import RectBivariateSpline
#from mpl_toolkits.mplot3d import Axes3D

# Regularly-spaced, coarse grid
#dx, dy = 0.4, 0.4
#xmax, ymax = 2, 4
#x = np.arange(-xmax, xmax, dx)
#y = np.arange(-ymax, ymax, dy)
#X, Y = np.meshgrid(x, y)
#Z = np.exp(-(2*X)**2 - (Y/2)**2)
#
#interp_spline = RectBivariateSpline(y, x, Z)
#
## Regularly-spaced, fine grid
#dx2, dy2 = 0.16, 0.16
#x2 = np.arange(-xmax, xmax, dx2)
#y2 = np.arange(-ymax, ymax, dy2)
#X2, Y2 = np.meshgrid(x2,y2)
#Z2 = interp_spline(y2, x2)
#
#fig, ax = plt.subplots(nrows=1, ncols=2, subplot_kw={'projection': '3d'})
#ax[0].plot_wireframe(X, Y, Z, color='k')
#
#ax[1].plot_wireframe(X2, Y2, Z2, color='k')
#for axes in ax:
#    axes.set_zlim(-0.2,1)
#    axes.set_axis_off()
#
#fig.tight_layout()
#plt.show()


#CamImage = plt.imread('test.png')
#CamImage.flags.writable = True
#plt.imshow(CamImage)
#image_red = CamImage[:,:,0]
#image_green = CamImage[:,:,1]
#image_blue = CamImage[:,:,2]

#a = CamImage[0:1000, 0:2000]
#plt.imshow(a)

#a = np.array([1, 2, 3])
#b = np.array([2, 3, 4])

#c = np.stack((a,b))


#from scipy.interpolate import RegularGridInterpolator
#def f(x, y):
#    return 2 * x**3 + 3 * y**2 
#
#x = np.linspace(1, 4, 11)
#y = np.linspace(4, 7, 22)
##z = np.linspace(7, 9, 33) 
#
##a = np.meshgrid(x, y, indexing = 'ij', sparse = True)
#data = f(*np.meshgrid(x, y, indexing = 'ij', sparse = True))
#
#a = (x, y)
#
#InterpolationFunction = RegularGridInterpolator(a, data)
#pts = np.array([[2.1, 6.2], [3.3, 5.2]])
#result = InterpolationFunction(pts)
#

#a = np.array([2.1, 6.2, 8.3])
#b = np.array([3.3, 5.2, 7.1])
#c = np.array([[2.1, 6.2, 8.3], [3.3, 5.2, 7.1]])
#d = np.stack((a, b))
#a 

#a = np.zeros((10,10))
#
#a[3:5,3:5] = np.array([[1,1], [1,1]])

#import tensorflow as tf
#tf.InteractiveSession()
#
#
##a = tf.zeros((2, 2))
##print(a.eval())
#with tf.variable_scope("foo"):
#    v = tf.get_variable("v", [1])
#    tf.get_variable_scope().reuse_variables()
#    v1 = tf.get_variable("v", [1])
#
#assert v1 == v
#    
#p