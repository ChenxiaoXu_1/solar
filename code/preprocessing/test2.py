#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 14:34:40 2018

@author: chenxiaoxu
"""

import numpy as np
import os, glob
from matplotlib import pyplot as plt
import stat_tools as st
from PIL import Image
import geopy
from geopy.distance import VincentyDistance
from scipy.interpolate import RegularGridInterpolator

inpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180515/code/preprocessing/' 
outpath = '/Users/chenxiaoxu/Dropbox/BNL/Research/Solar/HuangDongCode/20180515/code/preprocessing/' 

Him_name = inpath + 'HD815_1' + '_20180308153951.jpg'
Rim_name = inpath + 'HD490' + '_20180308153951.jpg'   

Him = plt.imread(Him_name)
Him = np.flip(Him, 0)
Him = np.flip(Him, 1)

Rim = plt.imread(Rim_name)
Rim = np.flip(Rim, 0)
Rim = np.flip(Rim, 1)

Hadj = np.zeros(Him.shape, dtype = np.uint8)

for i in range(0,3):
    HimMask = Him[:,:,i] > 0
    HimMask = 1*HimMask
        
    HimVec = np.reshape(Him[:,:,i], (Him.shape[0] * Him.shape[1], -1))
    RimVec = np.reshape(Rim[:,:,i], (Rim.shape[0] * Rim.shape[1], -1))    
    
    R = np.histogram(RimVec, bins = np.arange(0,257))[0]
    H = np.histogram(RimVec, bins = np.arange(0,257))[0]
    HimVec = np.reshape(Him[:,:,i], (Him.shape[0] * Him.shape[1], -1))
    RimVec = np.reshape(Rim[:,:,i], (Rim.shape[0] * Rim.shape[1], -1))
   
    Rc = np.cumsum(R)
    Hc = np.cumsum(H)    
    
    Hmap = [np.where(Rc >= val)[0][0] for val in Hc]
    
#    a = np.zeros(Him.shape[0:2], dtype = np.uint8)   
    for j in range(0, Him.shape[1]):
        Hadj[:,j,i] = [Hmap[val] for val in Him[:,j,i]]
        
    a = Hadj[:,:,i]
        
#    Hadj[:,:,i] = Hadj[:,:,i]*HimMask
        
    
    
#plt.imshow(Hadj, origin = 'lowest')


#R1 = np.histogram(RimVec[:, 0], bins = 255)
