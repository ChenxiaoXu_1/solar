clear all
close all
clc
Him = importdata('HD815_1_20180308153951.jpg');
Rim = importdata('HD490_20180308153951.jpg');

R = histc(reshape(Rim, size(Rim,1)*size(Rim, 2), []), 0:255);
H = histc(reshape(Him, size(Him,1)*size(Him, 2), []), 0:255);

% figure(1)
% subplot(2, 1, 1);
% plot(R)
% subplot(2, 1, 2);
% plot(H)

Rc = cumsum(R);
Hc = cumsum(H);

   
Hmap = arrayfun(@(val) find(Rc >= val, 1), Hc);
Hadj = uint8(Hmap(Him(:, : ,1)+1)-1);

Hadj = HistNorm(Rim, Him);
% figure(2)
% image(Hadj);
% figure(3)
% image(Him);
% figure(4)
% image(Rim);

R = histc(reshape(Rim, size(Rim,1)*size(Rim, 2), []), 0:255);
H = histc(reshape(Hadj, size(Him,1)*size(Him, 2), []), 0:255);

% figure(5)
% subplot(2, 1, 1);
% plot(R)
% subplot(2, 1, 2);
% plot(H)